<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList" %>
<jsp:useBean id="viewEventAll" class="com.ifw.Bean.EventBean" scope="session" />  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <jsp:include page="desk_head.jsp"></jsp:include>
    <jsp:include page="desk_script.jsp"></jsp:include>
        <link href="js/social.core.css" rel="stylesheet">

        <body>
        <%--<jsp:include page="../header.jsp"></jsp:include>--%>
        <div id="wrapper">

            <!-- Navigation -->
            <!--  <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">  -->

            <div class="navbar-header1">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <!--   <a class="navbar-brand" href="index.html">SB Admin v2.0</a> -->
            </div>
            <!-- /.navbar-header -->


            <!-- /.navbar-top-links -->
            <jsp:include page="desk_menu.jsp"></jsp:include>


                <!-- 		</nav> -->

                <div style="min-height: 356px;" id="page-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Event</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>


                    <div class="row">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Area Chart Example
                                    <div class="pull-right">
                                        <div class="btn-group">
                                            <button type="button"
                                                    class="btn btn-default btn-xs dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Actions <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <form novalidate="novalidate" id="advanced-form" class="form-horizontal">
                                <jsp:include page="desk_Event_submenu.jsp"></jsp:include>
                                    <div class="panel-body">
                                       
                                        <c:forEach items="${sessionScope.viewEvent}" var="viewEvent"> 
                                            <div class=" portlet box blue">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        ${viewEvent.id}<i class="fa fa-reorder"></i>${viewEvent.eventType} -------  ${viewEvent.eventId}
                                                    </div>
                                                    <div class="actions">
                                                        ${viewEvent.empId}
                                                        <a href="#" class="btn yellow btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                                                        <a href="#" class="btn green btn-sm"><i class="fa fa-plus"></i> Delete</a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <table>

                                                        <tr><td rowspan="2" style="padding: 5px 5px 5px 5px;">
                                                                <img src="js/background.jpg" width="120px" height="100px" />
                                                            </td>
                                                            <td style="padding: 5px 5px 5px 5px;">${viewEvent.subject} ------ ${viewEvent.organizeFor}</td>
                                                        </tr>
                                                        <tr><td colspan="2" style="padding: 5px 5px 5px 5px;">
                                                               ${viewEvent.description}
                                                            </td>
                                                            
                                                        </tr>
                                                    </table>

                                                </div>
                                            </div>
                                             
                                        </c:forEach> 
                                          


                                   
                                </div>
                               
                            </form>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Bar Chart Example
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button"
                                                class="btn btn-default btn-xs dropdown-toggle"
                                                data-toggle="dropdown">
                                            Actions <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-clock-o fa-fw"></i> Responsive Timeline
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <jsp:include page="desk_NotificationPanel.jsp"></jsp:include>
                    <!-- /.col-lg-8 -->

                    <!-- /.col-lg-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->


    </body>
</html>