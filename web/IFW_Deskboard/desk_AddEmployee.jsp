<%-- 
    Document   : Admin_desk_home
    Created on : Jan 20, 2015, 7:43:46 PM
    Author     : CHIRAG
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.java.ifw.util.DbConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
            <jsp:include page="desk_head.jsp"></jsp:include>
<jsp:include page="desk_script.jsp"></jsp:include>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      
 <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/style.css">
        <link rel="stylesheet" href="dist/css/style-metronic.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="dist/css/BeatPicker.min.css">
        
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/BeatPicker.min.js"></script>
<%request.getParameter("isEmpInserted");
if(request.getParameter("isEmpInserted")!=null)
{
if(request.getParameter("isEmpInserted").equals("true"))
{
%>
<script type="text/javascript">
alert("You are Add Employee Successfully...");
</script>

<%}else{%>
<script type="text/javascript">
alert("Please Insert Correct Value...");
</script>	
<%}}%>
<script type="text/javascript">
    
    
    
        function ajax()
{
	//alert("in ajax"+$("#deptId").val());
	 $.ajax({
	  	
	    	type:"GET",
	    	url:"../PoAjaxServlet",
	    data:{ departmentId:$("#deptId").val() }
	    
	    	
	    		
	    	
	    })
	      .done(function(msg)
	    {
	    	//alert(msg);
	    	$("#position").html(msg);
	    	
	    	
	    }		
	    
	    );
	 
	

	}
        </script>

        <title>JSP Page</title>
    </head>
    <body>
      
       <div id="wrapper">

		
         
		<div class="navbar-header1">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			
		</div>
		
<jsp:include page="desk_menu.jsp"></jsp:include>
<div style="min-height: 356px;" id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Dashboard</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
		
			<div class="row">
				<div class="col-lg-8">
					<div class="panel panel-default">

          <form action="../AddEmployeeServlet"  novalidate="novalidate" id="advanced-form" class="form-horizontal">
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-reorder"></i>Add Employee
                                            </div>
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                                <a href="javascript:;" class="remove"></a>
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->

                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="firstname" class="col-md-4 control-label">Employee Id</label>
                                                    <div class="col-md-4">
                                                        <input name="empId" id="empId" placeholder="Employee Id" class="form-control input-md" type="text">
                                                    </div>
                                                </div>
                                                
                                                

                                                <div class="form-group">
                                                    <label for="firstname" class="col-md-4 control-label">Department Name</label>
                                                    <div class="col-md-4">
                                                        <select class="form-control input-md"  name="deptId" id="deptId" onchange="ajax()">
                                                   <option class="form-control input-md">--select</option>
                                                                                                             <%
                           
                          PreparedStatement pre=null;
                          Connection cn=null;
                         String selectDepartment="Select * from department"; 
                        System.out.print("in dept");
                    cn=new DbConnection().getConnection();
                    pre=cn.prepareStatement(selectDepartment);
                         ResultSet rs=pre.executeQuery();
                         while(rs.next())
                         {
                        	System.out.print("in loop") ;
                               System.out.print("data"+rs.getString("deptId"));
                                
                        %>
                        <option value="<%out.print(rs.getString("deptId"));%>" class="form-control"><%out.print(rs.getString("deptName")); %></option>
                              
                        	 
                     <%    } %>
                                                        </select>
                                          </div>
                                               
                                                </div>
                                                         <div class="form-group">
                                                    <label for="firstname" class="col-md-4 control-label">Position Name</label>
                                                    <div class="col-md-4">
                                                        <select class="form-control input-md"  name="position" id="position" >
                                                   <option class="form-control input-md">--select</option>
                                                      </select>
                                          </div>
                                               
                                                </div>
                                                            <div class="form-group">
                                                                                <label for="lastname" class="col-md-4 control-label">Email Id</label>
                                                                                <div class="col-md-4">
                                                                                    <input name="emailId" id="emailId" placeholder="EmailId" class="form-control input-md" type="text">
                                                                                </div>
                                                                            </div>
                                                       <div class="form-group">
                                                                              <label for="lastname" class="col-md-4 control-label">Password</label>
                                                                                <div class="col-md-4">
                                                                                    <input name="pass" id="pass" placeholder="Password" class="form-control input-md" type="password">
                                                                                </div>
                                                                            </div>
                                                               <div class="form-group">
                                                                                <label for="lastname" class="col-md-4 control-label">Date Of Join</label>
                                                                                <div class="col-md-4">
                                                                                                        <input data-beatpicker-id="beatpicker-0" style="background-image: url(&quot;data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAANAA0DASIAAhEBAxEB/8QAFgABAQEAAAAAAAAAAAAAAAAABgQF/8QAIxAAAQQBBAIDAQAAAAAAAAAAAQIDBBEFABIhQQYxExQiUf/EABQBAQAAAAAAAAAAAAAAAAAAAAX/xAAbEQACAgMBAAAAAAAAAAAAAAABIQAEFUFhMf/aAAwDAQACEQMRAD8Af5PxCZHiwYeRzUBuMwHPqoU0sJRdqXzVXwa9cCheqT4dl8zj4RXm4smIy3sjflwbU3yOe+K6raB1p/k8VFyTjJl/MoNhaQhLhSk7hRsD2a761lePSnV4dCo+1ptLzraUrtZoKuyomySSST/TpHK2EwuDfsOxddovp1P/2Q==&quot;);" readonly="readonly" class="beatpicker-input beatpicker-inputnode" data-beatpicker="true" type="date" name="doj" class="form-control input-md">

                                                                                </div>
                                                                            </div>
                                                                            
                                                 <div class="panel-footer">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-offset-3 col-sm-9">
                                                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                                                <button type="button" class="btn btn-danger">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                
                                

                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </form>
 
				</div>
                                    
        
					<!-- /.panel -->
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-clock-o fa-fw"></i> Responsive Timeline
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
                                   <jsp:include page="desk_NotificationPanel.jsp"></jsp:include>
			</div>
    </div>
			<!-- /.row -->
		
		<!-- /#page-wrapper -->
             
    </body>
</html>
