<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <jsp:include page="desk_head.jsp"></jsp:include>
    <jsp:include page="desk_script.jsp"></jsp:include>

        <script type="text/javascript">
            $(document).ready(function () {
                alert("load");
                /* $("#chathead").click(function() {
                 
                 }); */
            });

        </script>
        <body>
        <%--<jsp:include page="../header.jsp"></jsp:include>--%>
        <div id="wrapper">

            <!-- Navigation -->
            <!--  <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">  -->

            <div class="navbar-header1">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <!--   <a class="navbar-brand" href="index.html">SB Admin v2.0</a> -->
            </div>
            <!-- /.navbar-header -->

          
            <!-- /.navbar-top-links -->
            <jsp:include page="desk_menu.jsp"></jsp:include>
               
                <!-- /.navbar-static-side -->


                <!-- 		</nav> -->

                <div style="min-height: 356px;" id="page-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Profile</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                   
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="panel panel-default">

                                <!-- /.panel-heading -->
                                <form action="../ProfileServlet.do" method="post" novalidate="novalidate" id="advanced-form" class="form-horizontal">
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-reorder"></i>Manage Your Profile
                                            </div>
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                                <a href="javascript:;" class="remove"></a>
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->

                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="firstname" class="col-md-4 control-label">First Name</label>
                                                    <div class="col-md-4">
                                                        <input id="firstname" name="firstname" placeholder="Your First Name" class="form-control input-md" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lastname" class="col-md-4 control-label">Last Name</label>
                                                    <div class="col-md-4">
                                                        <input id="lastname" name="lastname" placeholder="Your Last Name" class="form-control input-md" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="address" class="col-md-4 control-label">Address</label>
                                                    <div class="col-md-4">
                                                        <input id="address" name="address" placeholder="Enter your address" class="form-control input-md" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="mobile" class="col-md-4 control-label">Mobile</label>
                                                    <div class="col-md-4">
                                                        <input id="mobile" name="mobileNo" placeholder="Enter a valid Mobile Number" class="form-control input-md1" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="birthdate" class="col-md-4 control-label">Birthday</label>
                                                    <div class="col-md-4 input-parent input-container">
                                                        <div class="input-parent input-container"><input data-beatpicker-id="beatpicker-0" name="dob" style="background-image: url(&quot;data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAANAA0DASIAAhEBAxEB/8QAFgABAQEAAAAAAAAAAAAAAAAABgQF/8QAIxAAAQQBBAIDAQAAAAAAAAAAAQIDBBEFABIhQQYxExQiUf/EABQBAQAAAAAAAAAAAAAAAAAAAAX/xAAbEQACAgMBAAAAAAAAAAAAAAABIQAEFUFhMf/aAAwDAQACEQMRAD8Af5PxCZHiwYeRzUBuMwHPqoU0sJRdqXzVXwa9cCheqT4dl8zj4RXm4smIy3sjflwbU3yOe+K6raB1p/k8VFyTjJl/MoNhaQhLhSk7hRsD2a761lePSnV4dCo+1ptLzraUrtZoKuyomySSST/TpHK2EwuDfsOxddovp1P/2Q==&quot;);" readonly="readonly" class="beatpicker-input beatpicker-inputnode" data-beatpicker="true" type="text"></div></div>
                                                </div>
                                                <div class="form-group">
                                            <label class="col-md-4 control-label">Gender</label>
                                            <div class="col-md-4">
                                                <div class="radio">
                                                    <label for="gender-0">
                                                        <input id="gender-0" name="gender" value="m" checked="checked" type="radio">Male
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label for="gender-1">
                                                        <input id="gender-1" name="gender" value="f" type="radio">Female
                                                    </label>
                                                </div>

                                            </div>
                                        </div>

                                                <div class="form-group">
                                                    <label for="Country" class="col-md-4 control-label">Country</label>
                                                    <div class="col-md-4">
                                                        <input id="Country" name="country" placeholder="Enter your country" class="form-control input-md" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="State" class="col-md-4 control-label">State</label>
                                                    <div class="col-md-4">
                                                        <input id="State" name="state" placeholder="Enter your State" class="form-control input-md" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="City" class="col-md-4 control-label">City</label>
                                                    <div class="col-md-4">
                                                        <input id="City" name="city" placeholder="Enter your City" class="form-control input-md" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Pincode" class="col-md-4 control-label">Pincode</label>
                                                    <div class="col-md-4">
                                                        <input id="Pincode" name="pincode" placeholder="Enter your Pincode" class="form-control input-md" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputFile" class="col-md-34 col-md-4 control-label">File input</label>
                                                    <div class="col-md-9 col-md-4 control-label">
                                                        <input id="exampleInputFile" type="file" name="profileImage">

                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-3 col-sm-9">
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                            <button type="button" class="btn btn-danger">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>



                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </form>



                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Bar Chart Example
                                    <div class="pull-right">
                                        <div class="btn-group">
                                            <button type="button"
                                                    class="btn btn-default btn-xs dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Actions <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">

                                    <!-- /.row -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-clock-o fa-fw"></i> Responsive Timeline
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">

                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-8 -->
                    <jsp:include page="desk_NotificationPanel.jsp"></jsp:include>
                    <!-- /.col-lg-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->


    </body>
</html>