<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<!--[if IE 9]>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<![endif]-->


<link rel="stylesheet" id="contact-form-7-css" href="profile_assets/styles.css" type="text/css" media="all">
<link rel="stylesheet" id="sktbuybtn-css" href="profile_assets/sktbuybtn.css" type="text/css" media="all">
<link rel="stylesheet" id="convac-style-css" href="profile_assets/style.css" type="text/css" media="all">
<link rel="stylesheet" id="convac-animation-stylesheet-css" href="profile_assets/skt-animation.css" type="text/css" media="all">
<link rel="stylesheet" id="convac-flexslider-stylesheet-css" href="profile_assets/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" id="sktawesome-theme-stylesheet-css" href="profile_assets/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" id="sktconvac-font-stylesheet-css" href="profile_assets/convac-font.css" type="text/css" media="all">
<link rel="stylesheet" id="ihover-theme-stylesheet-css" href="profile_assets/ihover.css" type="text/css" media="all">
<link rel="stylesheet" id="convac-prettyPhoto-style-css" href="profile_assets/prettyPhoto.css" type="text/css" media="all">
<link rel="stylesheet" id="convac-blog-masonry-css" href="profile_assets/blog-masonry.css" type="text/css" media="all">
<link rel="stylesheet" id="sktddsmoothmenu-superfish-stylesheet-css" href="profile_assets/superfish.css" type="text/css" media="all">
<link rel="stylesheet" id="bootstrap-responsive-theme-stylesheet-css" href="profile_assets/bootstrap-responsive.css" type="text/css" media="all">
<link rel="stylesheet" id="googleFontsOpensans-css" href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C600&amp;ver=1.0.2" type="text/css" media="all">
<link rel="stylesheet" id="skt-shortcodes-css-css" href="profile_assets/shortcodes.css" type="text/css" media="all">
<link rel="stylesheet" id="skt-tolltip-css-css" href="profile_assets/tipTip.css" type="text/css" media="all">
<script src="profile_assets/ga.js" async="" type="text/javascript"></script>
<script src="profile_assets/ga.js" async="" type="text/javascript"></script><script type="text/javascript" src="profile_assets/jquery.js"></script>
<script type="text/javascript" src="profile_assets/jquery-migrate.js"></script>
<script type="text/javascript" src="profile_assets/sktbuybtn.js"></script>
<!-- <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-includes/wlwmanifest.xml">  -->
<meta name="generator" content="WordPress 4.0.1">
<!-- Start analytics code 16/10/2014 3:51:02 PM -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-48146016-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- End analytics code 16/10/2014 3:51:09 PM -->
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

<style>

	/***************** THEME *****************/
	
	#wrapper .hsearch .row-fluid { background: rgba(26,188,156,.95);}
	#wrapper {background:#eeeded}
	
		
			html .skt-blog-share.single-blog-share{display:block;}
		
	#logo img{width:128px;height:24px}
	.bread-title-holder,#footer,.widget_tag_cloud a:hover,a#backtop,.continue a:hover,form input[type="submit"]:hover{ /* color:#fff;background: #1abc9c; */}
	
	h1, h2, h3, h4, h5, h6,.food_map_window .food_right_sec h3,.contact_form_title,#contact_page_temp .contact_top_block h3,blockquote.sketch-quote a,.quoteauthor a,blockquote,#blog-masonry #content .post_excerpt::first-letter,#blog-masonry #content .masonry-citation-post .quoteauthor a{color: #1abc9c;}
	.error404 #searchform input[type="text"]:focus, .search #searchform input[type="text"]:focus, #sidebar #searchform input[type="text"]:focus, #footer #searchform input[type="text"]:focus,#wp-calendar,.sound-cloud-audio-container .skt-format-audio iframe,.convac-post-box .citation_post,.convac-post-box .audio-container,#convac-paginate .convac-current,#convac-paginate a:hover,.convac-post-box .skt-link-container,#about_author_box .author_avtar,#about_author_box .author-content-box,#about_author_box .border-au-title,#content .commentlist .avatar,.skt_price_table .price_table_inner .price_button a{border-color: #1abc9c;}	
	#about_author_box .author_avtar .author_avtar_rob:after{border-left-color: #1abc9c;}
	li.ui-timepicker-selected, .ui-timepicker-list li:hover, .ui-timepicker-list .ui-timepicker-selected:hover,#wp-calendar thead{background: #1abc9c;}	
	.sticky-post,#blog-masonry #content .masonry-link-container,#blog-masonry #content .masonry-audio-container {color : #1abc9c;border-color:#1abc9c}
	.social li a:hover,.sketch_price_table .price_table_inner ul li.table_title,._404_artbg img,.skt-subs-widget input[type="submit"],.skepost-meta > span,#convac-paginate .convac-current,#convac-paginate a:hover{background: #1abc9c;}
	.social li a:hover:before{color:#fff; }
	
	#respond input[type="submit"],.skt-ctabox div.skt-ctabox-button a:hover,.continue a,.comments-template .reply a,#commentsbox .reply a,.full-map-box .hsearch-close,form input[type="submit"]  {border:1px solid #1abc9c; color:#1abc9c}
	
	.postformat-gallerycontrol-nav a.postformat-galleryactive,#wp-calendar tbody a,.conv-tags .fa.fa-tag,.conv-category .fa.fa-folder,.skt_price_table .price_table_inner .price_button a:hover,.skt_price_table .price_table_inner ul li.prices {background-color: #1abc9c; }
	.skt-ctabox div.skt-ctabox-button a,.slider-link a,.ske_tab_v ul.ske_tabs li.active,.ske_tab_h ul.ske_tabs li.active,#content .contact-left form input[type="submit"],.filter a,#convac-paginate a:hover,#convac-paginate .convac-current,form.wpcf7-form input[type="text"]:focus,form.wpcf7-form input[type="email"]:focus,
	form.wpcf7-form input[type="url"]:focus,form.wpcf7-form input[type="tel"]:focus,
	form.wpcf7-form input[type="number"]:focus,form.wpcf7-form input[type="range"]:focus,
	form.wpcf7-form input[type="date"]:focus,form.wpcf7-form input[type="file"]:focus,form.wpcf7-form textarea:focus,
	form input[type="text"]:focus,form input[type="email"]:focus,
	form input[type="url"]:focus,form input[type="tel"]:focus,
	form input[type="number"]:focus,form input[type="range"]:focus,
	form input[type="date"]:focus,form input[type="file"]:focus,form textarea:focus,#skenav ul ul,.widget_tag_cloud a,#blog-masonry #content .masonry-citation-post .skt-quote{border-color:#1abc9c;}

	a,.ske_widget ul ul li:hover:before,.ske_widget ul ul li:hover,.ske_widget ul ul li:hover a,.title a ,.skepost-meta a:hover,.post-tags a:hover,.entry-title a:hover ,.readmore a:hover,#Site-map .sitemap-rows ul li a:hover ,.childpages li a,#Site-map .sitemap-rows .title,.ske_widget a:hover,#Site-map .sitemap-rows ul li:hover,#footer .third_wrapper a,.ske-title,#content .contact-left form input[type="submit"],.filter a,span.team_name,.reply a, a.comment-edit-link, .teammember strong .team_name,#content .skt-service-page .one_third:hover .service-box-text h3,.ad-service:hover .service-box-text h3,.mid-box-mid .mid-box:hover .iconbox-content h4,.error-txt,.skt-ctabox .skt-ctabox-content h2,.reply a:hover, a.comment-edit-link:hover,.skepost-meta i,.topbar_info i, .topbar_info .head-phone-txt {color: #1abc9c;text-decoration: none;}
	.single #content .title,#content .post-heading,.childpages li ,.fullwidth-heading,.comment-meta a:hover,#respond .required,h3#comments-title, h3#reply-title,.nav-previous,.nav-next,#comments,.full-map-box .hsearch-close:hover,#convac_review .convac_review_form_title,.cust-review-title,#reviewer_review_box i,.single-meta-content span,.iconbox-icon i,.customer-reviews .review-menuitem,.post.skt_menu_items .menu-item-price span,.sketch_price_table .price_in_table .value,.widget_tag_cloud a ,.skt-link-container .format-link,.skt-link-container .skt-format-link p,.skt-link-container .skt-format-link a,.skepost > p:first-child::first-letter,.skepost::first-letter,#main .ske_widget ul .skt-recent-posts li span:before {color: #1abc9c;} 

	*::-moz-selection{background: #1abc9c;color:#fff;}
	::selection {background: #1abc9c;color:#fff;}

	.sticky-post { border-color: #1abc9c;  }
	#searchform input[type="submit"]{ background: none repeat scroll 0 0 #1abc9c;  }

	.col-one .box .title, .col-two .box .title, .col-three .box .title, .col-four .box .title {color: #1abc9c !important;  }
	#full-division-box { background-image: url("http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/french-fries-226773_1280.jpg"); }
	.footer-top-border {border: 2px solid #1abc9c;}
	.front-page #wrapper{background: none repeat scroll 0 0 rgba(0, 0, 0, 0); }

	
	/******************* HEADER BACKGROUND IMAGE ******************/

	.header-top-wrap { background: url("http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/uploads/sites/66/2014/07/Header-Image-Inside.png") no-repeat scroll top center transparent;background-size: cover;}	
	.home .header-top-wrap,body.page-template-template-blog-left-sidebar-page-php .header-top-wrap,body.page-template-template-masonry-php .header-top-wrap,body.page-template-template-blog-full-width-php .header-top-wrap,body.page-template-template-masonry-php .header-top-wrap,body.page-template.page-template-template-blog-right-sidebar-page-php .header-top-wrap{ background: url("http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/uploads/sites/66/2014/07/Header-Image.png") no-repeat scroll top center transparent;-webkit-background-size: cover;-moz-background-size: cover ;-o-background-size: cover ; background-size: cover ;}	
	#full-twitter-box { background: url("http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/uploads/sites/66/2014/07/Twitter-band-bg.png") no-repeat center center fixed transparent;-webkit-background-size: cover;-moz-background-size: cover ;-o-background-size: cover ; background-size: cover ;}	
	
	/************** CONTACT PAGE HEADER BACKGROUND IMAGE **********/

		.page-template-template-contact-page-php .header-top-wrap {background:none;}
		
		
	/******************** CONTACT PAGE HEIGHT ********************/
		blockquote.sketch-quote,blockquote{margin-bottom: 27px;border: 1px solid #1abc9c; border-right: 5px solid  #1abc9c; } 

	/********************** MAIN NAVIGATION **********************/
	#skenav li a:hover,#skenav .sfHover{ color: #fff;}
	#skenav ul ul li{ background: none repeat scroll 0 0 #222222; color: #FFFFFF; }
	#skenav ul ul li a:hover{background-color: rgba(26,188,156,1);color:#fff;}
	#skenav ul ul:after{border-bottom-color:#1abc9c;}
			
	@media only screen and (max-width : 1025px) {
		#menu-main {
			display:none;
		}
		#header .container {
			width:97%;
		}
	}

</style>

<script type="text/javascript">
jQuery('document').ready(function(){
	jQuery('ul#menu-main').sktmobilemenu({'fwidth':'1025'});
});

</script>	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>