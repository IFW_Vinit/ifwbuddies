package com.ifw.Servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
//import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import com.inuxu.DAO.GOSSDataManager;
//import com.inuxu.Utillities.SurveyUtility;

/**
 * Servlet implementation class CouponUpServlet
 */
@WebServlet("/CouponUpServlet")
public class CouponUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private boolean isMultipart;
	private String filePath;
	private String filePathdir;

	private int maxFileSize = 1024 * 500;
	private int maxMemSize = 4 * 1024;
	private File file;
	//private static Logger logger = Logger.getLogger(CouponUpServlet.class);
	private static final String webapp = "IFWBuddies";
	private static final String imgapp = "BuddiesBucket";

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	public CouponUpServlet() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		//logger.info("in init");

		//SurveyUtility.initlog4j(config);

		try {

			/*
			 * DataProviderOld.getInstance().initDataProvider(config);
			 */
		} catch (Exception e) { 
			e.printStackTrace();
		}

	}

	/**
	 * @throws SQLException
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException,
			SQLException {
		
		response.setContentType("text/html");
		String clientOrigin = request.getHeader("origin");
		//logger.info("clientOrigin with o: " + clientOrigin);
		response.addHeader("Access-Control-Allow-Origin", clientOrigin);
		response.addHeader("Access-Control-Allow-Credentials", "true");
		try{
			Properties prop = new Properties();
			InputStream input = null;
			input = FileUploaderServlet.class.getClassLoader().getResourceAsStream("com/ifw/Utils/db_properties.properties");
			prop.load(input);
			
			//String pubmailId = prop.getProperty("pubId");
			//String couponmailId = prop.getProperty("couponmailId");
			String couponsize = prop.getProperty("Couponsize");
			String MemSize = prop.getProperty("MemSize");
			
			ServletContext servletContext = getServletContext();
			String realPathimg = servletContext.getRealPath("/");
			
			String realPath = realPathimg.replace(webapp, imgapp);
			
			// Application Path:D:\Vinit\GOSS V2\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\wtpwebapps\SurveySys\
			//logger.info("Application Path:" + realPath);
			
			//1// filePath = realPath + "images/publisher/";
			filePathdir =realPath + "images/publisher/";
	       // filePath = realPath + "images/publisher/";
			//logger.info("filePathdir" + filePathdir);
			try {
				File pathObj = new File(realPath);
				File pathimgObj = new File(pathObj+"/images");
				File pathimg = new File(pathimgObj+"/publisher"); 
				  // if the directory does not exist, create it
				  if (!pathObj.exists())
				  {
					 // logger.info("creating directory: " + filePathdir);
					  pathObj.mkdir();
					  
				  }
				  if (!pathimgObj.exists())
				  {
					//  logger.info("creating directory: " + filePathdir);
					  pathimgObj.mkdir();
					  
				  }
				  if (!pathimg.exists())
				  {
					 // logger.info("creating directory: " + filePathdir);
					  pathimg.mkdir();
					  
				  }
				 
				 
			} catch (Exception e) {
			
				e.printStackTrace();
				//logger.error("Failed to check for existing directory", e);
			
			}
			
			request.getQueryString();
			String filename = request.getParameter("photo");
			String couponname = request.getParameter("couponname");
			String pubname = request.getParameter("publisherId");
			//System.out.println(couponname + "   " + pubname);
			String pub_dir=  filePathdir + pubname+"/";
			filePath = pub_dir;
			//logger.info("File name :-"+filename);
			
			File dirObj = new File(pub_dir);
			 
			  // if the directory does not exist, create it
			  if (!dirObj.exists())
			  {
				 // logger.info("creating directory: " + pub_dir);
			      dirObj.mkdir();
			  }
			  else
			  {
				 // logger.info(pub_dir + "directory already existed: ");
			  }	 
			  
			    isMultipart = ServletFileUpload.isMultipartContent(request);
				response.setContentType("text/html");
				//logger.info("isMultipart :- " + isMultipart);

				java.io.PrintWriter out = response.getWriter();
				if (!isMultipart) {

					return;
				}
				DiskFileItemFactory factory = new DiskFileItemFactory();

				// maximum size that will be stored in memory

				// factory.setSizeThreshold(maxMemSize);
				factory.setSizeThreshold(maxFileSize);
				// Location to save data that is larger than maxMemSize.
				// factory.setRepository(new File("c:\\temp"));

				// Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);

				// maximum file size to be uploaded.
				upload.setSizeMax(maxFileSize);
				try {
					// Parse the request to get file items.
					List fileItems = upload.parseRequest(request);

					// Process the uploaded file items
					Iterator i = fileItems.iterator();

					while (i.hasNext()) {
		              
						FileItem fi = (FileItem) i.next();
						
						
						if (!fi.isFormField()) {

							// Get the uploaded file parameters
							String fieldName = fi.getFieldName();
							String fileName = fi.getName();
							if(fieldName.equals("photo"))
							{
								if(couponname.equals("banner1"))
			                    {
			                    	fileName = fileName.replace(fi.getName(), "01.png");
			                    }
			                    if(couponname.equals("banner2"))
			                    {
			                    	fileName = fileName.replace(fi.getName(), "02.png");
			                    }
			                    if(couponname.equals("banner3"))
			                    {
			                    	fileName = fileName.replace(fi.getName(), "03.png");
			                    }
			                    if(couponname.equals("logoimg"))
			                    {
			                    	fileName = fileName.replace(fi.getName(), "logo.png");
			                    }
								
								//logger.info("File Name:-"+fileName);
								/*String ok = fi.getString();
			                     System.out.println("hi print ing"+fi.getString());*/
								String contentType = fi.getContentType();
								boolean isInMemory = fi.isInMemory();
								long sizeInBytes = fi.getSize();
								// Write the file
								if (fileName.lastIndexOf("\\") >= 0) {
									file = new File(
											filePath
													+ fileName.substring(fileName
															.lastIndexOf("\\")));
								} else {
									file = new File(
											filePath
													+ fileName.substring(fileName
															.lastIndexOf("\\") + 1));
								}

								fi.write(file);
								//logger.info(fileName+ "succeessfully uploaded.");
							}
						
		                   /* GOSSDataManager.SendSimpleMessage(couponmailId, pubmailId);
							logger.info("Uploded file" + fileName);*/
		                   
						}
					}
				} catch (Exception ex) {
					//logger.error("Failed to upload coupon ",ex);
					ex.printStackTrace();

				}
		}catch(Exception ex)
		{
			//logger.error("Failed to upload coupon ",ex);
			ex.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (Exception e) {
			//logger.info("Failed to upload coupon.", e);
		}
	}

}
