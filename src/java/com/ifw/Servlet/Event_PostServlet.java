/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.Servlet;

import com.ifw.Bean.DepartmentBean;
import com.ifw.Bean.EventBean;
import com.ifw.Dao.EventDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Vinit
 */
public class Event_PostServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            HttpSession sessionObj = request.getSession(true);
            sessionObj.setAttribute("employeeId", "ifw01");
            String empId = (String) sessionObj.getAttribute("employeeId");
           
             System.out.println("imgs daat" + request.getParameter("eventimage"));
             EventDao eventManagerObj = new EventDao();
             String eventId = eventManagerObj.eventIdGen();
            
             EventBean eventBeanObj = new EventBean();
             eventBeanObj.setEventId(eventId);
             eventBeanObj.setEmpId(empId);
             eventBeanObj.fillBean(request);
             
//          
//             ServletContext servletContext = getServletContext();
//             RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher("/FileUploaderServlet.do");
//             requestDispatcher.include(request, response);
//         
//             
             if(eventManagerObj.postEvent(eventBeanObj))
             {
              String nextjsp="./IFW_Deskboard/desk_Event_View.jsp";
              response.sendRedirect(nextjsp);
             }
             else
             {
              String nextjsp="/Event_ViewServlet.do";
              response.sendRedirect(nextjsp);
             }   
             
	   
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
