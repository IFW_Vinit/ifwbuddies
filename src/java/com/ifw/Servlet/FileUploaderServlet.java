/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.Servlet;

// Import required java libraries
import com.ifw.Dao.EventDao;
import com.ifw.Dao.FileUploaderDao;
import java.io.*;
import java.util.*;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.*;

/**
 *
 * @author Vinit
 */
public class FileUploaderServlet extends HttpServlet {

    private boolean isMultipart;
    private String filePath;
    private String realPath;
    private String filePathdir;
    private String redirectUrl;
    private String isUpload = "false";
    private int maxFileSize = 5000 * 1024;
    private int maxMemSize = 40 * 1024;
    private File file;

    private static final String webapp = "IFWBuddies";
    private static final String imgapp = "BuddiesBucket";

    public void init() {
      // Get the file location where it would be stored.

        // filePath = getServletContext().getInitParameter("file-upload"); 
        ServletContext servletContext = getServletContext();
        String realPathimg = servletContext.getRealPath("/");

        realPath = realPathimg.replace(webapp, imgapp);
    }

    public void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, java.io.IOException {

        System.out.println("realPath" + realPath);

        request.getQueryString();
        redirectUrl = request.getParameter("redirectUrl");
        System.out.println("redirectUrl :- " + redirectUrl);

        filePathdir = realPath + "imag\\employ\\";
        System.out.println("filePathdir ; - " + filePathdir);
  
        FileUploaderDao fileUploaderDaoObj = new FileUploaderDao();
        fileUploaderDaoObj.makeDirectory(realPath);
        
        HttpSession sessionObj = request.getSession();
        sessionObj.setAttribute("employeeId", "ifw02");
        String empID = (String) sessionObj.getAttribute("employeeId");

        if (empID != null) {
            filePathdir = fileUploaderDaoObj.makeEmployDiectory(filePathdir, empID);

            // Check that we have a file upload request
            isMultipart = ServletFileUpload.isMultipartContent(request);
            response.setContentType("text/html");
            DiskFileItemFactory factory = new DiskFileItemFactory();
            // factory.setSizeThreshold(maxMemSize);
            //  factory.setRepository(new File("c:\\temp"));
            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(maxFileSize);

            try {
                List fileItems = upload.parseRequest(request);
                Iterator i = fileItems.iterator();
                while (i.hasNext()) {
                    FileItem fi = (FileItem) i.next();
                    if (!fi.isFormField()) {
                        String fieldName = fi.getFieldName();
                        String fileName = fi.getName();
                        String contentType = fi.getContentType();
                        boolean isInMemory = fi.isInMemory();
                        long sizeInBytes = fi.getSize();
                        // Write the file
                        if (fileName.lastIndexOf("\\") >= 0) {
                            file = new File(filePathdir
                                    + fileName.substring(fileName.lastIndexOf("\\")));
                        } else {
                            file = new File(filePathdir
                                    + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        }
                        fi.write(file);
                        isUpload = "true";

                    }
                }
                if (isUpload.equals("true") && redirectUrl != null) {
                    String redirestUrl = redirectUrl;
                    //setRedirection(request, response, redirestUrl);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {
            System.out.println("Employee Id Gatting  : - " + empID);
        }

    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void setRedirection(HttpServletRequest request, HttpServletResponse response, String redirectMainUrl)
            throws ServletException, java.io.IOException {
        String nextjsp = redirectMainUrl;
        // request.setAttribute("isValidCaptcha", "false");
        RequestDispatcher dispatcher = request
                .getRequestDispatcher(nextjsp);
        dispatcher.forward(request, response);
    }

//    public void callFileUploader(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, java.io.IOException {
//        init();
//        processRequest(request, response);
//    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
