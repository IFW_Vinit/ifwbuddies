/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ifw.Bean;

import java.sql.Date;





/**
 *
 * @author CHIRAG
 */
public class AddEmployeeBean {
    
    private String employeeId;
    private  String department;
     private  String position;
      private  String emailId;
     private  String password;
     private  Date  doj;

    public Date getDoj() {
        return doj;
    }

    public void setDoj(Date doj) {
        this.doj = doj;
    }

   

   

    

    public String getEmployeeId() {
        return employeeId;
    }

    public String getDepartment() {
        return department;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPosition() {
        return position;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getPassword() {
        return password;
    }

   
    
}
