/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ifw.Bean;

/**
 *
 * @author CHIRAG
 */
public class PositionBean {
    private String positionId;

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }
    private String positionName;
    private String departmentId;

    public String getPositionId() {
        return positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public String getDepartmentId() {
        return departmentId;
    }
}
