/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.Bean;

/**
 *
 * @author Vinit
 */
public class EventBean extends AbstractEntity{
    
    protected int id;
    protected String eventId;
    protected String empId;
    protected String eventType;
    protected String organizeFor;
    protected String subject;
    protected String description;
    protected String eventimage;

//    public EventBean(String eventID, String empId, String eventType, String organizeFor, String subject, String description, String image) {
//        this.eventID = eventID;
//        this.empId = empId;
//        this.eventType = eventType;
//        this.organizeFor = organizeFor;
//        this.subject = subject;
//        this.description = description;
//        this.image = image;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    
   // AddEmployeeBean employbj =new AddEmployeeBean();
    

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEmpId() {
        
       // return employbj.getEmployeeId();
        return empId;
    }

    public void setEmpId(String empId) {
       // employbj.setEmployeeId(empId);
        this.empId = empId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getOrganizeFor() {
        return organizeFor;
    }

    public void setOrganizeFor(String organizeFor) {
        this.organizeFor = organizeFor;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEventimage() {
        return eventimage;
    }

    public void setEventimage(String eventimage) {
        this.eventimage = eventimage;
    }

   
    
}
