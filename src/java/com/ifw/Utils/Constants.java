/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.Utils;

import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Vinit
 */
public class Constants {
    
      public static String DBMS_DRIVER="";
    public static String DBMS_URL="";
    public static String DBMS_USERNAME="";
    public static String DBMS_PASSWORD ="";
     
    
    public void getProperties()
    {
        
        try {
            Properties prop = new Properties();
		InputStream input = null;
		input = Constants.class.getClassLoader().getResourceAsStream(
				"com/ifw/Utils/db_properties.properties");
		try {
			prop.load(input);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//logger.error("SendSimpleMessage() :: Failed to read file", e);
		}

		DBMS_DRIVER = prop.getProperty("master_driver");
		DBMS_URL = prop.getProperty("master_url");
                DBMS_USERNAME = prop.getProperty("master_user");
		DBMS_PASSWORD = prop.getProperty("master_pass");
              // System.out.println("All :- " + DBMS_DRIVER + " "+ DBMS_URL + " "+ DBMS_USERNAME + " "+ " "+ DBMS_PASSWORD);
   
        } catch (Exception e) {
            e.printStackTrace();
        }
    }        
}
