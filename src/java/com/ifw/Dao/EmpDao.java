/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.Dao;
import com.ifw.Bean.*;
import com.ifw.Utils.*;
import java.sql.*;
import java.util.ArrayList;
/**
 *
 * @author Pratik
 */
public class EmpDao {
    
    DBMS d=new DBMS();
     public boolean addEmp(ProfileBean p){
        boolean flag=false;
        String sql="insert into profile_master(empid,firstname,lastname,gender,dob,address,profileImage,country,state,city,pincode,mobileNo) values('"+p.getEmpId()+"','"+p.getFirstname()+"','"+p.getLastname()+"','"+p.getGender()+"','"+p.getDob()+"','"+p.getAddress()+"','"+p.getProfileImage()+"','"+p.getCountry()+"','"+p.getState()+"','"+p.getCity()+"','"+p.getPincode()+"','"+p.getMobileNo()+"')";
        
        if(d.guid(sql)==1){
            flag=true;
        } 
        return flag;
}
     
    public ArrayList<ProfileBean> viewAllProfileData(String empId) {
        ResultSet rs = null;
        ArrayList<ProfileBean> viewProfileObj = new ArrayList<ProfileBean>();
        DBMS dbObj = new DBMS();

        try {
           String sql= "SELECT profileImage FROM profile_master where empid='"+empId+"'";
        
            rs = dbObj.gselect(sql);
            while (rs.next()) {
                ProfileBean Profile = new ProfileBean();
                Profile.setProfileImage(rs.getString("profileImage"));
                
                viewProfileObj.add(Profile);
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {

            dbObj.closeConnection();
        }

        return viewProfileObj;
    }
}
