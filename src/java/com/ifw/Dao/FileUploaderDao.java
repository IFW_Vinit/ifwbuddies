/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.Dao;

import java.io.File;

/**
 *
 * @author Vinit
 */
public class FileUploaderDao {

    public void makeDirectory(String realPath) {
        try {
            File pathObj = new File(realPath);
            File pathimgObj = new File(pathObj + "\\imag");
            File pathimg = new File(pathimgObj + "\\employ");
            // if the directory does not exist, create it
            if (!pathObj.exists()) {
                //  logger.info("creating directory: " + filePathdir);
                pathObj.mkdir();

            }
            if (!pathimgObj.exists()) {
                //  logger.info("creating directory: " + filePathdir);
                pathimgObj.mkdir();

            }
            if (!pathimg.exists()) {
                //  logger.info("creating directory: " + filePathdir);
                pathimg.mkdir();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String makeEmployDiectory(String filePathdir, String empID) {
        String emp_dir = "";
        try {
            emp_dir = filePathdir + empID + "/";

            System.out.println("Get Emp Dir :-" + emp_dir);
            File dirObj = new File(emp_dir);
            if (!dirObj.exists()) {
                System.out.println("creating directory: " + emp_dir);
                dirObj.mkdir();
            } else {
                System.out.println(emp_dir + "directory already existed: ");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return emp_dir;
    }
}
