/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.Dao;

import com.ifw.Bean.*;
import java.io.File;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vinit
 */
public class EventDao {
    private static final String VIEW_ALLEvent = "select * from event";
    private static final String VIEW_Department = "select * from department";
    private static final String GET_ALLEVENT = "select count(*) from event";

//---------method to view all Departments for Event  ----------//
    public ArrayList<DepartmentBean> viewAllDepartment() {
        ResultSet rs = null;
        ArrayList<DepartmentBean> viewDepartmentObj = new ArrayList<DepartmentBean>();
        DBMS dbObj = new DBMS();

        try {
            rs = dbObj.gselect(VIEW_Department);
            while (rs.next()) {
                DepartmentBean departmentObj = new DepartmentBean();
                departmentObj.setDeptId(rs.getString("deptId"));
                departmentObj.setDepartmentName(rs.getString("deptName"));
                viewDepartmentObj.add(departmentObj);
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {

            dbObj.closeConnection();
        }

        return viewDepartmentObj;
    }

    //---------method to view all Event  ----------//
    public ArrayList<EventBean> viewAllEvent() {
        ResultSet rs = null;
        ArrayList<EventBean> viewAllEventObj = new ArrayList<EventBean>();
        DBMS dbObj = new DBMS();

        try {
            rs = dbObj.gselect(VIEW_ALLEvent);
            while (rs.next()) {
                EventBean eventBeanObj = new EventBean();
                eventBeanObj.setId(rs.getInt("id"));
                eventBeanObj.setEventId(rs.getString("eventId"));
                eventBeanObj.setEmpId(rs.getString("empId"));
                eventBeanObj.setOrganizeFor(rs.getString("organizeFor"));
                eventBeanObj.setEventType(rs.getString("eventType"));
                eventBeanObj.setSubject(rs.getString("subject"));
                eventBeanObj.setDescription(rs.getString("description"));
                eventBeanObj.setEventimage(rs.getString("image"));

                viewAllEventObj.add(eventBeanObj);
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {

            dbObj.closeConnection();
        }

        return viewAllEventObj;
    }

    // Method to generate EventID  
    public String eventIdGen() {
        ResultSet rs;
        String j = "Evt";
        String id = "";
        DBMS dbObj = new DBMS();

        try {
            rs = dbObj.gselect(GET_ALLEVENT);
            rs.next();
            String i = String.valueOf(rs.getInt(1));
            int n = Integer.parseInt(i) + 1;
            String k = n + "";
            id = j.concat(k);
            System.out.println("id = " + id);

        } catch (Exception e) {

            e.printStackTrace();
        } finally {

            dbObj.closeConnection();
        }
        return id;
    }

    // method to add new event
    public boolean postEvent(EventBean evtObj) {
        DBMS dbPOSTObj = new DBMS();
        boolean flag = false;
        try {

            String sql = "INSERT INTO ifwonline.event(eventId,empId,eventType,organizeFor,subject,description,image)VALUES('" + evtObj.getEventId() + "','" + evtObj.getEmpId() + "','" + evtObj.getEventType() + "','" + evtObj.getOrganizeFor() + "','" + evtObj.getSubject() + "','" + evtObj.getDescription() + "','" + evtObj.getEventimage() + "')";

            if (dbPOSTObj.guid(sql) == 1) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            dbPOSTObj.closeConnection();
        }

        return flag;
    }

}
